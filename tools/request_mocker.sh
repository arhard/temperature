#!/bin/bash

counter=1
while [ true ]
do
    curl -X POST -H "Content-Type: application/json" -d '{"sensorId":"$counter"}' http://localhost:3000/api/temperature
    echo $counter
    ((counter++))
    sleep 1
done
