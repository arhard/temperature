import { Component, OnInit } from '@angular/core';
import { TemperatureService, TemperatureReading } from '../../services/temperature.service';

@Component({
  selector: 'app-temperature-list',
  templateUrl: './temperature-list.component.html',
  styleUrls: ['./temperature-list.component.css']
})
export class TemperatureListComponent implements OnInit {

  data: TemperatureReading[];

  constructor(private readonly service: TemperatureService) { }

  ngOnInit() {
    this.service.findAllReactive().subscribe(data => {
      this.data = <any>data;
      this.data = this.data.slice(0, 10);
    });
  }

  update() {
    this.service.findAllReactive().subscribe(data => {
      this.data = <any>data;
      this.data = this.data.slice(0, 10);
    })
  }

}
