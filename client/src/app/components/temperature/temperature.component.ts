import { Component, OnInit } from '@angular/core';
import { TemperatureService, TemperatureReading } from '../../services/temperature.service';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent implements OnInit {

  temperature: TemperatureReading;
  interval: any;
  constructor(private readonly service: TemperatureService) { }

  ngOnInit() {
    this.refreshData();
    this.interval = setInterval(() => {
      this.refreshData();
    }, 1000);
  }

  refreshData() {
    this.service.findLast().subscribe(data => {
      this.temperature = <TemperatureReading>data[0];
    });
  }

}
