import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';



const url = 'http://maciejdados.pl:3000/api/temperature'

export interface TemperatureReading {
  _id: string;
  sensorId: string;
  temperature: string;
  humidity: string;
  date: Date;
}

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {

  constructor(private readonly http: HttpClient) { }

  async findAll() {
    return this.http.get(url).toPromise();
  }

  findAllReactive() {
    return this.http.get(url);
  }

  findLast() {
    return this.http.get(`${url}/newest`);
  }
}
