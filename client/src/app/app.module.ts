import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { AppComponent } from './app.component';
import { TemperatureListComponent } from './components/temperature-list/temperature-list.component';
import { TemperatureComponent } from './components/temperature/temperature.component';


@NgModule({
  declarations: [
    AppComponent,
    TemperatureListComponent,
    TemperatureComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
