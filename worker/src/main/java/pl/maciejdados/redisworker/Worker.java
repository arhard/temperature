package pl.maciejdados.redisworker;

import pl.maciejdados.redisworker.redis.RedisService;

public class Worker {
    public static void main(String args[]) {
       final RedisService redisService = RedisService.INSTANCE;
       redisService.process();
    }
}