package pl.maciejdados.redisworker.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.BSON;
import org.bson.Document;
import org.json.JSONObject;

import pl.maciejdados.redisworker.dto.TemperatureReadingDto;

public class MongoService {
    public final static MongoService INSTANCE = new MongoService("mongodb://mongo:27017");
    private final MongoClient mongoClient;

    private MongoService(String host) {
        System.out.println("Connecting to mongodb://mongo:27017");
        MongoClientOptions.Builder options = new MongoClientOptions.Builder().socketKeepAlive(true);
        // MongoClientOptions options = (new MongoClientOptions).Builder().socketKeepAlive(true).build(); 
        mongoClient = new MongoClient(new MongoClientURI(host, options));
    }

    public void process(TemperatureReadingDto dto) {
        try {

            MongoDatabase database = mongoClient.getDatabase("temperatures");
            MongoCollection<Document> document = database.getCollection("temperatures");
            document.insertOne(
                new Document()
                    .append("sensorId", dto.getSensorId())
                    .append("temperature", dto.getTemperature())
                    .append("humidity", dto.getHumidity())
                    .append("date", dto.getDate())
            );
            
            System.out.println("PERSISTED");
        } catch (Exception e) {
            System.err.println("An error has occured in MongoSerivce process(): " + e.getMessage());
        }
    }
    

}