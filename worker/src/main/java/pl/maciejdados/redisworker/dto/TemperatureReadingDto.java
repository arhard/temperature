package pl.maciejdados.redisworker.dto;

import org.json.JSONObject;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TemperatureReadingDto {
    private String sensorId;
    private String temperature;
    private String humidity;
    private String date;

    public static TemperatureReadingDto fromJSONObject(JSONObject jsonObject) {
        TemperatureReadingDto dto = new TemperatureReadingDto(
            jsonObject.getString("sensorId"),
            jsonObject.getString("temperature"),
            jsonObject.getString("humidity"),
            jsonObject.getString("date")
        );

        return dto;
    }
}