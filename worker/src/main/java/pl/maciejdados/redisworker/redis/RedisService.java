package pl.maciejdados.redisworker.redis;

import org.json.JSONObject;

import pl.maciejdados.redisworker.dto.TemperatureReadingDto;
import pl.maciejdados.redisworker.mongo.MongoService;
import redis.clients.jedis.Jedis;

public class RedisService {
    public static final RedisService INSTANCE = new RedisService("redis");
    private final MongoService mongoService = MongoService.INSTANCE;
    private final Jedis redis;

    private RedisService(String host) {
        System.out.println("Connecting to redis");
        this.redis = new Jedis(host);
    }

    public void process() {
        System.out.println("RedisService proces() started");
        while(true) {
            String json = redis.blpop(0, "temperatures").get(1);
            JSONObject data = new JSONObject(json);
            System.out.println(data);
            System.out.println("temperatureReading " + json);
            mongoService.process(TemperatureReadingDto.fromJSONObject(data));
        }
    }

}