import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as cors from 'cors';
/**
 * Bootstraps application
 */
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // middlewares
  app.use(cors());
  
  // for validating request
  app.useGlobalPipes(new ValidationPipe());

  // swagger configuration
  const options = new DocumentBuilder()
    .setTitle('Temperature reading server')
    .setDescription('Server processes request and puts them into redis list for later processing.')
    .setVersion('1.0')
    .addTag('api/temperature')
    .build();


  // register documentation
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  // start server
  await app.listen(3000);
}
bootstrap();
