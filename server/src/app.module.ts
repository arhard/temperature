import { Module } from '@nestjs/common';
import { TemperatureModule } from 'temperature/temperature.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [TemperatureModule, MongooseModule.forRoot('mongodb://mongo:27017/temperatures')],

})
export class AppModule {}
