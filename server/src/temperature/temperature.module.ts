import { Module } from "@nestjs/common";
import { TemperatureController } from "./controllers/temperature.controller";
import { TemperatureService } from "./services/temperature.service";
import { MongooseModule } from "@nestjs/mongoose";
import { TemperatureReadingSchema } from "./schemas/temperature-reading.schema";


@Module({
    imports: [MongooseModule.forFeature([{name: 'Temperature', schema: TemperatureReadingSchema}])],
    controllers: [TemperatureController],
    providers: [TemperatureService],
})
export class TemperatureModule {}
