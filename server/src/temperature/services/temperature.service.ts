import { Injectable } from "@nestjs/common";
import * as Redis from 'ioredis';
import { TemperatureReading } from "../interfaces/temperature-reading.interface";
import { TemperatureReadingDto } from "../dto/temperature-reading.dto";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

/**
 * Redis key of list that stores temperatures
 */
const key = 'temperatures';

/**
 * Temperature serivce provides methods to insert temperatures to and read them from redis
 */
@Injectable()
export class TemperatureService {
    /**
     * Redis instance
     */
    private redis = new Redis(6379, 'redis');
    
    constructor(@InjectModel('Temperature') private readonly temperatureModel: Model<TemperatureReading>) {}

    /**
     * Inserts value into redis list
     * @param value temperature reading to be inserted into redis
     * @returns inserted value
     */
    create(value: TemperatureReadingDto): TemperatureReading {
        value.date = new Date();
        this.redis.rpush(key, JSON.stringify(value)); 
        
        return value as TemperatureReading;
    }

    /**
     * Returns all temperatures from mongodb
     * @returns temperatures
     */
    async findAll() {
        return this.temperatureModel.find({}).sort({date: -1}).limit(1000); 
    }

    /**
     * Returns newest temperature log
     * @returns newest temperature
     */
    async findLast() {
        return this.temperatureModel.find({}).sort({date: -1}).limit(1);
    }

}