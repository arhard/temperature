import * as mongoose from 'mongoose';

export const TemperatureReadingSchema = new mongoose.Schema({
    sensorId: String,
    temperature: String,
    humidity: String,
    date: Date
});

