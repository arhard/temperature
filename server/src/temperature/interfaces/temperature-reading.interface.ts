import { ApiModelProperty } from "@nestjs/swagger";
import { Document } from "mongoose";

export interface TemperatureReading extends Document {
    readonly sensorId: string;
    readonly temperature: string;
    readonly humidity: string;
    readonly date?: Date;
}