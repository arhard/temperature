
import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { IsString, IsDate, IsOptional } from "class-validator";

/**
 * DTO for temperature reading, decorated with validators
 */
export class TemperatureReadingDto {

    @ApiModelProperty()
    @IsString()
    readonly sensorId: string;

    @ApiModelProperty()
    @IsString()
    readonly temperature: string;

    @ApiModelProperty()
    @IsString()
    readonly humidity: string;

    @ApiModelPropertyOptional()
    @IsOptional()
    @IsDate()
    date?: Date;
}