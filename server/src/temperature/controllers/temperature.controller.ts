import { Controller, Post, Body, Get } from "@nestjs/common";
import { TemperatureService } from "../services/temperature.service";
import {
    ApiUseTags,
    ApiResponse,
    ApiOperation
} from '@nestjs/swagger';
import { TemperatureReadingDto } from "../dto/temperature-reading.dto";


/**
 * Temperature controller
 */
@ApiUseTags('api/temperature')
@Controller('api/temperature')
export class TemperatureController {
   
    /**
     * Constructor
     * @param temperatureService serivce used for making operations on temperature readings
     */
    constructor(private readonly temperatureService: TemperatureService) {}


    /**
     * Creates temperature reading
     * @param temperature Temperature reading to be later inserted into db by a worker
     */
    @Post()
    @ApiOperation({title: 'Create temperature reading'})
    @ApiResponse({
        status: 201,
        description: 'Temperature reading has been successfully crated.'
    }) 
    create(@Body() temperature: TemperatureReadingDto) {
        return this.temperatureService.create(temperature);
    }


    /**
     * Returns all temperatures
     * @returns temperatures
     */
    @Get()
    @ApiOperation({title: 'Reads saved temperatures'})
    @ApiResponse({
        status: 200,
        description: 'Reads temperatures from mongodb which were saved by worker'
    })
    async findAll() {
        return await this.temperatureService.findAll();
    }

    /**
     * Returns all temperatures
     * @returns temperatures
     */
    @Get('/newest')
    @ApiOperation({title: 'Returns newest temperature'})
    @ApiResponse({
        status: 200,
        description: 'Reads newest temperature from mongodb which were saved by worker'
    })
    async findLast() {
        return await this.temperatureService.findLast();
    }



    
    
}